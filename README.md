Docker CentOS Base
==================

[Docker Hub](https://hub.docker.com/r/cvadmins/centos-base/)
[GitLab](https://gitlab.com/CollegiumV/docker-centos-base)

The Docker CentOS base container serves as a base container to test Ansible deployment. Primarily designed for automated testing in a fresh environment, using a CI/CD platform for remote testing and `docker-compose` for local testing.

Docker Compose
--------------

`docker-compose` can be used for local testing. Running `docker-compose up --build` will rebuild the centos-base container and test it with the `test.sh` script from an `ubuntu:latest` container.

GitLab CI
---------

GitLabCI is used to build and deploy the container to DockerHub.

1) The container is built and pushed to the internal GitLab container registry.

2) The container is run as a service and tested to verify Ansible can run on the container remotely.

3) If tagged, the container is pushed to Dockerhub.
