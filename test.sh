#!/bin/sh
# This script is designed to be run on an Ubuntu docker container
apt-get update -qq && apt-get install -y -qq openssh-client sshpass python-pip
pip install -r requirements.txt
ansible-playbook all.yml
